# Design Pattern In Java

> **What is Design Pattern?**

Software Design Pattern can be defined as a software template or a description to solve a problems that occurs multiple times while designing a Software Application or a Software Framework. 
* A standard solution to a common programming problem.
  - A design or implementation structure that achieves a particular purpose.
  - A high-level programming idiom.
> **Why do we used design pattern?**

1. **Flexibility:** Using design patterns your code becomes flexible.
2. **Reusability:** A design pattern provides a general reusable solution for the common problems occurs in software design. It provides the solution to the problem that already solved in the past and we can re-use it without caring about the solution.
3. By learning these patterns and the related problem, an inexperienced developer learns a lot about software design.

### Type of design pattern
There are mainly three types of design patterns:
1. **Creational Design Patterns.**
2. **Structural Design Patterns.**
3. **Behaviour Design Patterns.**

> **Note: In this, we are mainly going to discuss about the Creational Design Pattern.**

#### **Creational Design Patterns**
**Creational design** patterns are **design patterns** that deal with object creation mechanisms, trying to create objects in a manner suitable to the situation. Creational design patterns are used to design the instantiation process of objects. The creational pattern uses the inheritance to vary the object creation.
**The Creational Design Patterns are further classified as follows:**
1. **Factory Pattern**
2. **Abstract Factory Pattern**
3. **Singleton Pattern**
4. **Prototype Pattern**
5. **Builder Pattern**


##### 1.1. Factory Pattern
- The Factory Design Pattern or Factory Method Design Pattern is one of the most used design patterns in Java.
- This pattern means defines an interface for creating an object, but let subclasses decide which class to instantiate. 
- This pattern delegates the responsibility of initializing a class from the client to a particular factory class by creating a type of virtual constructor.
- To achieve this, we rely on a factory which provides us with the objects, hiding the actual implementation details. The created objects are accessed using a common interface.

###### **Factory Method Design Pattern Example**
In this example, we'll create a OperatingSystem interface which will be implemented by several concrete classes. A OperatingSystemFactory class will be used to fetch objects from this family:

```java
public interface OperatingSystem{
    void spec();
}
```
The above interface will be used by the different Operating System class.
```java
public class Android implements OperatingSystem{
    public void spec(){
        System.out.println("Hi i am Android");
    }
}
```
The Android implements the OperatingSystem and is used to show the spec.
```java
public class IOS implements OperatingSystem{
    public void spec(){
        System.out.println("Hi i am IOS");
    }
}
```
The IOS implements the OperatingSystem and is used to show the spec.
```java
public class Window implements OperatingSystem{
    public void spec(){
        System.out.println("Hi i am Window");
    }
}
```
The Window implements the OperatingSystem and is used to show the spec.

```java
public class OperatingSystemFactory{
    public OperatingSystem getInstance(String str){  //String as Type of OperatingSystem
        if(str.equals("Android"){
            return new Android();
        }else if(str.equals("IOS"){
            return new IOS();
        }else if(str.equals("Window"){
            return new Window();
        }
        return null;
    }
}
```
The above class return the required type of object.
```java
public class TypeOfOS{
    public void static main(String[] args){
        OperatingSystemFactory osf =new OperatingSystemFactory();
        OperatingSystem operatingSystem = osf.getInstance("Android");
        operatingSystem.spec();
    }
}
```
Output:
```
Hi i am Android
```
> **Note: We have create class "OperatingSystemFactory" which return the type of os and work as a factory which works as manufacture and create the desired objects.**

Think, If in future we have an extra class than we can perform the change in OperatingSystemFactory class rather than modifying the main class.

##### 1.2. Abstract Factory Pattern
**Abstract Factory** pattern is almost similar to **Factory Pattern** is considered as another layer of abstraction over factory pattern. Abstract Factory patterns work around a super-factory which creates other factories.

In this example, we'll create two implementations of the Factory Method Design pattern: AnimalFactory and ColorFactory.

![Abstract Factory Design Pattern Example](https://www.baeldung.com/wp-content/uploads/2018/11/updated_abstract_factory.jpg)
First, we'll create a family of Animal class and will, later on, use it in our Abstract Factory.
**Here's the Animal interface:**
```java
public interface Animal {
    String getAnimal();
    String makeSound();
}
```
**and a concrete implementation Duck:**
```java
public class Duck implements Animal {
 
    @Override
    public String getAnimal() {
        return "Duck";
    }
 
    @Override
    public String makeSound() {
        return "Squeks";
    }
}
```

**Here's the color interface:**
```java
public interface Color {
    String getColor();
    String changeColor();
}
```
**and a concrete implementation White:**
```java
public class White implements Color {
 
    @Override
    public String getColor() {
        return "White";
    }
 
    @Override
    public String changeColor() {
        return "Orange";
    }
}
```
Furthermore, we can create more concrete implementations of Animal interface (like Dog, Bear, etc.) and color interface like(Brown,Red) exactly in the manner.

Now that we've got multiple families ready,
```java
public interface AbstractFactory<T> {
    T create(String animalType) ;
}
```
Next, we'll implement an AnimalFactory using the Factory Method design pattern that we discussed in the previous section:
```java
public class AnimalFactory implements AbstractFactory<Animal> {
 
    @Override
    public Animal create(String animalType) {
        if ("Dog".equalsIgnoreCase(animalType)) {
            return new Dog();
        } else if ("Duck".equalsIgnoreCase(animalType)) {
            return new Duck();
        }
 
        return null;
    }
 
}
```
**Note: Similarly, we can implement a factory for the Color interface using the same design pattern.**

 we'll create a FactoryProvider class that will provide us with an implementation of AnimalFactory or ColorFactory depending on the argument that we supply to the getFactory() method:
 
 ```java
 public class FactoryProvider {
    public static AbstractFactory getFactory(String choice){
        
        if("Animal".equalsIgnoreCase(choice)){
            return new AnimalFactory();
        }
        else if("Color".equalsIgnoreCase(choice)){
            return new ColorFactory();
        }
        
        return null;
    }
}
 ```
#### 1.3. singleton pattern
The **singleton pattern** is one of the simplest design patterns. Sometimes we need to have only one instance of our class for example, a single DB connection shared by multiple objects as creating a separate DB connection for every object may be costly.

> **The singleton pattern is a design pattern that restricts the instantiation of a class to one object.**

For Example: 
```java
class Singleton 
{ 
    private static Singleton obj; 
  
    // private constructor to force use of 
    // getInstance() to create Singleton object 
    private Singleton() {} 
  
    public static Singleton getInstance() 
    { 
        if (obj==null) 
            obj = new Singleton(); 
        return obj; 
    } 
} 
```
Here we have declared getInstance() static so that we can call it without instantiating the class. The first time getInstance() is called it creates a new singleton object and after that it just returns the same object. Note that Singleton obj is not created until we need it and call getInstance() method.

#### 1.4. Prototype Pattern

In Object Oriented Programming, you need objects to work with; objects interact with each other to get the job done. But sometimes, creating a heavy object could become costly, and if your application needs too many of that kind of objects (containing almost similar properties), it might create some performance issues.

The concept is to copy an existing object rather than creating a new instance from scratch, something that may include costly operations. The existing object acts as a prototype and contains the state of the object. The newly copied object may change some properties only if required. This approach saves costly resources and time, especially when the object creation is a heavy process.Every user object has an access control object, which is used to provide or restrict the controls of the application. 
This access control object is a bulky, heavy object and its creation is very costly since it requires data to be fetched from some external resources, like databases or some property files etc
**Prototype Design Participants**
1. **Prototype** : This is the prototype of actual object.
2. **Prototype registry** : This is used as registry service to have all prototypes accessible using simple string parameters.
3. **Client** : Client will be responsible for using registry service to access prototype instances.
```java
import java.util.HashMap; 
import java.util.Map; 

abstract class Color implements Cloneable 
{ 
      
    protected String colorName; 
       
    abstract void addColor(); 
       
    public Object clone() 
    { 
        Object clone = null; 
        try 
        { 
            clone = super.clone(); 
        }  
        catch (CloneNotSupportedException e)  
        { 
            e.printStackTrace(); 
        } 
        return clone; 
    } 
} 

class blueColor extends Color 
{ 
    public blueColor()  
    { 
        this.colorName = "blue"; 
    } 
   
    @Override
    void addColor()  
    { 
        System.out.println("Blue color added"); 
    } 
      
} 

class blackColor extends Color{ 
   
    public blackColor() 
    { 
        this.colorName = "black"; 
    } 
   
    @Override
    void addColor()  
    { 
        System.out.println("Black color added"); 
    } 
} 

class ColorStore { 
   
    private static Map<String, Color> colorMap = new HashMap<String, Color>();  
       
    static 
    { 
        colorMap.put("blue", new blueColor()); 
        colorMap.put("black", new blackColor()); 
    } 
       
    public static Color getColor(String colorName) 
    { 
        return (Color) colorMap.get(colorName).clone(); 
    } 
} 
```
```java
class Prototype 
{ 
    public static void main (String[] args) 
    { 
        ColorStore.getColor("blue").addColor(); 
        ColorStore.getColor("black").addColor(); 
        ColorStore.getColor("black").addColor(); 
        ColorStore.getColor("blue").addColor(); 
    } 
} 
```
&nbsp;
Output:
```
Blue color added
Black color added
Black color added
Blue color added
```

#### 1.5. Builder Pattern

The builder pattern, as name implies, is an alternative way to construct complex objects.
Builder pattern solves the issue with large number of optional parameters and inconsistent state by providing a way to build the object step-by-step and provide a method that will actually return the final Object.
**Let’s see how we can implement builder design pattern in java.**

1. First of all you need to create a static nested class and then copy all the arguments from the outer class to the Builder class. We should follow the naming convention and if the class name is Computer then builder class should be named as ComputerBuilder.
2. Java Builder class should have a public constructor with all the required attributes as parameters.
3. Java Builder class should have methods to set the optional parameters and it should return the same Builder object after setting the optional attribute.
4. The final step is to provide a build() method in the builder class that will return the Object needed by client program. For this we need to have a private constructor in the Class with Builder class as argument.

```java

public class Computer {
	
	//required parameters
	private String HDD;
	private String RAM;
	
	//optional parameters
	private boolean isGraphicsCardEnabled;
	private boolean isBluetoothEnabled;
	

	public String getHDD() {
		return HDD;
	}

	public String getRAM() {
		return RAM;
	}

	public boolean isGraphicsCardEnabled() {
		return isGraphicsCardEnabled;
	}

	public boolean isBluetoothEnabled() {
		return isBluetoothEnabled;
	}
	
	private Computer(ComputerBuilder builder) {
		this.HDD=builder.HDD;
		this.RAM=builder.RAM;
		this.isGraphicsCardEnabled=builder.isGraphicsCardEnabled;
		this.isBluetoothEnabled=builder.isBluetoothEnabled;
	}
	
	//Builder Class
	public static class ComputerBuilder{

		// required parameters
		private String HDD;
		private String RAM;

		// optional parameters
		private boolean isGraphicsCardEnabled;
		private boolean isBluetoothEnabled;
		
		public ComputerBuilder(String hdd, String ram){
			this.HDD=hdd;
			this.RAM=ram;
		}

		public ComputerBuilder setGraphicsCardEnabled(boolean isGraphicsCardEnabled) {
			this.isGraphicsCardEnabled = isGraphicsCardEnabled;
			return this;
		}

		public ComputerBuilder setBluetoothEnabled(boolean isBluetoothEnabled) {
			this.isBluetoothEnabled = isBluetoothEnabled;
			return this;
		}
		
		public Computer build(){
			return new Computer(this);
		}

	}

}

```
> **Note: Notice that Computer class has only getter methods and no public constructor. So the only way to get a Computer object is through the ComputerBuilder class.**

Here is a builder pattern example test program showing how to use Builder class to get the object.



```java
public class TestBuilderPattern {

	public static void main(String[] args) {
		//Using builder to get the object in a single line of code and 
                //without any inconsistent state or arguments management issues		
		Computer comp = new Computer.ComputerBuilder(
				"500 GB", "2 GB").setBluetoothEnabled(true)
				.setGraphicsCardEnabled(true).build();
	}

}
```
Reference:
- [JournalDev](https://www.journaldev.com/1827/java-design-patterns-example-tutorial)
- [Geeksforgeeks](https://www.geeksforgeeks.org/prototype-design-pattern/)
- [Java Design Pattern](http://enos.itcollege.ee/~jpoial/java/naited/Java-Design-Patterns.pdf)




